import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput,Button,TouchableOpacity,Image,TouchableHighlight} from 'react-native';
import ListItems from './components/ListItems';
import placeList from './components/PlaceList';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
    state = {
        placeName: '',
        places: [],
    };

    frizInputHandler = value=> {
        return this.setState({placeName: value})
    };

    testButton() {
        // Alert('test button');
        // console.log('this is test button');
        // alert('this is test button')
    };

    addText() {
        if (this.state.placeName.trim() === '') {
            alert('you cannot add empty string');
            return
        }
        else {

            //setState({places: this.state.places.concat(this.state.placeName)})
            //
            //setState((prevState)=>({places: prevState.places.concat(prevState.placeName)}))

            this.setState((prevState)=> {
                return {
                    places: prevState.places.concat(prevState.placeName)
                }
            })

            //[1,2,3].concat(4)
            //[1,2,3,4].concat([4,5,6,7])
            //[12344567]
        }
    };

    onItemDeleted=(index)=> {
        alert('on delete tap');
        this.setState(prevState=> {
            return {
                places: prevState.places.filter((place, i)=> {
                    return i !== index;
                })
            }
        })
    }

    render() {
        const placeOutPut = this.state.places.map((place, i)=>(
            <ListItems key={i} placeName={place} onItemPressed={this.onItemDeleted.bind(this,i)}/>

        ));
        return (
            <View style={styles.container}>
                <View style={{flex:1}}>

                </View>
                <View style={{flex:4}}>
                    <View style={styles.frizInputContainer}>
                        <TextInput style={styles.frizInput}
                                   onChangeText={this.frizInputHandler}
                                   value={this.state.placeName}
                                   placeholder="input text"
                        />
                        {/*<Button onPress={this.testButton.bind(this)} title={'add'}/>*/}
                        <TouchableOpacity style={styles.TouchableOpacityContainer} onPress={this.addText.bind(this)}>
                            {/*<Image*/}
                            {/*style={styles.button}*/}
                            {/*source={require('./images/Facebook.png')}*/}
                            {/*/>*/}
                            <Text style={styles.button2}>add</Text>
                        </TouchableOpacity>

                    </View>
                    <View style={styles.placeOutPutStyle}>
                        {placeOutPut}
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    frizInputContainer: {
        // width:'100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    frizInput: {
        borderColor: 'black',
        borderRadius: 25,
        borderWidth: 1,
        width: '70%',
    },
    TouchableOpacityContainer: {
        // width:'30%',
        justifyContent: 'center',
        // textAlign: 'center',
        // alignContent:'center',

    },
    button: {
        borderWidth: 1,
        borderRadius: 25,
        width: 50,
        height: 50,
    },
    button2: {
        borderWidth: 1,
        width: 60,
        height: 50,
        borderRadius: 25,
        paddingTop: 10,
        textAlign: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    },
    placeOutPutStyle: {
        paddingTop: 5,
    }
});
