import React from 'react';
import {Platform, StyleSheet, Text, View,TouchableOpacity} from 'react-native';

const ListItems =(props)=>(
    <TouchableOpacity  onPress={props.onItemPressed}>
        <View style={styles.listProperty}>
                <Text style={styles.textStyle}>{props.placeName} </Text>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    listProperty: {
        marginTop:10,
        height: 50,
        width:'100%',
        backgroundColor: '#eee',
        borderWidth:1,
        borderRadius:25,
        justifyContent: 'center',
    },
    textStyle:{
        textAlign:'center',
    }

});

export default ListItems;